# Frequently Asked Questions (FAQ)

### General

What is ComposeableDAO (C|D)?
C|D is a protocol governed by C|D token holders. C|D welcomes all individuals and communities to join us and contribute. Find out more at: Introduction

Is C|D a registered organization?

C|D is governed fully by token holders who agree to adhere to laws set by smart contracts which run on the blockchain.

What is a DAO and why does C|D use DAO structure?

What kind of DAOs does C|D fund?

What is the E1155+ standards?

How does C|D protect its users? ephemeral app

What are C\D tokens?
C|D tokens are governance tokens of the DAO.

What is PAIR?
PAIR is a stable coin used to leverage the C|D.

How are C|D and PAIR tokens supplied?

Where can I store C|D or PAIR tokens?

What about lock up periods?

How does this DAO compare to DAOStack and Aragaon?





### Token Distribution


 Private Sale, Public Sale, Development, Liquitidity( C|D-PAIR trades), Ecosystem Fund (reserved for AIRDROPs,bounty &/ POW, other community reward programs), DAO creation

 All usage will be executed only after a DAO proposal and vote.

How are PAIR tokens distributed?

PAIR Tokens distributed on the 5TH and 20TH of each month, must be claimed within 3 days or may lose value each day thereafter. Value is lost at 2.5% per day. Tokens are completely forfeited after 180 days.

What happens to tokens once forfeited?

Tokens default to a split of thirds back into ecosystem fund, DAO creations, and burning. Users have the option to elect their personal preferences for how tokens are forfeited twice per year.


### Contribution Bounties

### Tokenomics

Supply and Inflation
(how much is the cap?)

C|D Address?

PAIR Address?

What are the staking(contracts) pools?

pools and their Weight of pools
ETH-pool
ETC-PAIR pool
LBTC-PAIR  pool
BTC-PAIR pool
C|D-PAIR

flash pools---new announced pools

What are the differences in the pools?



### Governance

Who owns C|D? Who controls the C|D's treasury?

C|D  is decentralized which anyone who becomes a token holder can own C|D. Token holders control the treasury.

How do I Participate in C|D?

By owning C|D you can participate in C|D initiatives. Quarterly Sales,stake through PAIR tokens. For more on how participation works see Governance.



