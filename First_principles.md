
1. The top Blockchains require Execution Environment bridges (today: BTC/ETH)
2. An algorithmic stable coin is necessary for all trading pairs in order to decentralize liquidity & create robust financial instruments 
3. Financial system stability is largely determined by the base  used to price asset indices
4. Composability encompasses front-end, middleware coordination (JavaScript) & smart contract (EVM/DLC/PTLC) execution environments
5. Composable DeFi protocols, ephemeral apps, & isomorphic execution are the basis for privacy, security, scalability as well as flexibility 
6. Individual open source contributors (coders, documenters, quality assurers, project managers, etc.) will  be rewarded in crypto currency directly proportional to their contributions through embedded bounty systems 
7. An instrumented Internet will emerge via crypto-enabled reward systems to support a global network management framework 
