PAIR Algorithm

PAIR Stablecoin

PAIR works as a stable coin in two ways. First, by being backed by silver and gold at current silver gold ratio value. Each pair token is backed by one gram of silver and gold. The gram ratio corresponds to the current market weighted value of gold and silver. 

As Silver is deposited into vaults,VAu NFT receipts are created.  Likewise as Gold is deposited, VAg NFT receipts. These combine to form the whole value of 1 PAIR. Second, by minting through the PAIR equation, which follows:


PAIR gram = {  [1/ (current gold silver ratio) ]  x  current price of 1 gram of gold } + { [ 1 -  (1 /  (current gold silver ratio)  ]  x  current $ 1 gram of silver }

1. Background

a. primal financial metric -- silver/gold ratio
b. market manipulation mechanism
c. creating non-fiat approach to prevent manipulation

Centralized commodities exchchanges such as the (COMEX)[https://en.wikipedia.org/wiki/New_York_Mercantile_Exchange] are vulnerable as has been extensively documented by (GATA)[https://gata.org]. The fiat currency system allows for a virtually infinite amount of value to be leveraged for the short term manipulation of the futures market. In essence, physical metal does not actually get traded between parties and instead profits and losses are settled exclusively in fiat. This manipulation results in stable prices in dollar terms with sharp jumps in price when either the official sector no longer can suppress the price or chooses to allow an increase in market prices when market psychology is favorable to their interests. Decentralizing physical metal exchange is critical to restoring a free market. In order to do so, a non-fiat based system must be established for the exchange to insure the printing press cannot be used to manipulate the market. New DeFi protocols provide the technology to enable such a system. Under the PAIR system, only silver may be exchanged for gold and vice versa. Therefore, any external futures contract or price manipulation via the settlement system requires physical exchange of the two metals. Effectively, the exchange of the underlying commodities can only be made algorithmically to mint, redeem, and establish the silver-gold ratio within the PAIR token. Arbitrageurs participate in the system to earn yield by setting the value 

Users can also mint PAIR using a Point of Sale (POS) like system called the iMynCoin. This unit runs applications over the [Compose Virtual Machine or CVM](./Composable_Framework.md).  Cryptocurrencies such as Lightning Bitcoin (LBTC), Ethereum(ETC), Ethereum Classic(ETC) and others may be swappped within the system using [PAIRx](./PAIRx.md). 

2. PAIR Composition

3. Minting

4. Smart Contract Implementation

5. Vault Operations

6. Arbitrage & Trading

7. Redemption

8. Non-Rehypothecation & Fully Collateralized Derivatives

9. Rebasing & Futures

10. Future Direction

Conversion fees or gas, occur as PAIR is minted. Stability fees occur as PAIR is held in addition to the PAIRx oracle service.
